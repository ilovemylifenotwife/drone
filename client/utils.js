const { ApolloClient, InMemoryCache, gql } = require('@apollo/client');
const axios = require('axios');
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const fetch = require('node-fetch');
const { createHttpLink } = require('apollo-link-http');
const Cors = require('cors');
const initMiddleware = require('../lib/init-middleware')
const HASURA_URL ='https://wd-hasura.tbps.in/v1/graphql';
const JWT_PRIVATE_KEY= '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCnj0zcyrqnXUJnbSBdIKSp6pNgExx6AxaPS123n72LrctolLpgvUkxGho+6jv9r4QlP2dHL1Ur8UIQraeB0wNIr+G2Ehcn4c2hct96z4ROZZ3djODHX7Wxl0vSLhnp9TBM8v5BVFAnaztmSZcEvv+Hh36RC+L1OyYFdX8w981JKYU1zz8gMqjjlzNs2D5kyQNV2nprErYZL5EcimESrP/mdGcmtU/GFYt8L1jihEohPK+bO1mXLFD87WPSvDbNNKkMzZ5eXsdd0BRQmlGCT6Rwzlniv1pBdNoidsvAOeDrZY6x2LbpjU1o5Z1tRciUJH7r4hrcFL2R1z40IZNUs+xLAgMBAAECggEBAJGr85bAaSW33lMsfEZ6aq2OUjHka41nweUyEHfjWFZm8cAqDR6tcURO/SJVYXJbdPcs+AzbCaD6TgVR857aZLB8uC4h7B+wH6WMyN1sLAo8LdK+92xduQh8EF7Z1Y2grC050/HX6xdaEnoK4FAv/NdG5LIVM2sf0R5N2BYqWT6QgGlG4WlVzOoyJhE2BE9lmumIwyKyHTEum5OQWu/msWQ4QsNu+JE+vwTBadXLQhbde8yI1rcZe2yEzoD3IVqqS11NjQZH+8oq6DCCw89A41f2QVzXY0dQIWr/j/N1YjMX9nYG7URi0Z8iSbZVx8EZ/q9UV789ttlyJEi0scAJFwECgYEA5L2ckw78/hsOZBGRubzWzm/TyxVqj+14MYYCGRYzIulBW49uZjNRqsJdHG8ANzi7AJ98afhpTGgU+e0qaYmTD8vbMrmljYhuUHy4Oei1EQjcTi4hHI7NZQQAWsBFRtF1+XY6BeRX3I4l2gqoivHT57BaG3OpdPulV3pp1JV4e3kCgYEAu4cwrt615AH6ieVSWu+BAW3hrTLEQjFqGzYAobQlvwiojc2LzQgzTleeH02tznWg3y0vbdvwcOR/lIBlJdMSORpNUJh60O+8Q4ZWLMxorCsVX6i3i7RK0vUMtsqW1QNRGDmwyv5g+qifzeoOnqeODr5bd03d/hOaOUc6iavF8OMCgYEA3S5H1593nRJ2gObIPYzPPQC/EDHeP9wFDAYjwzRHMCvSigGQY/ydrHuS42qmD7+oe3q8KBAPadV+6wicqT5hWLXreAoxfkD9QpDG5yAQ7a7esC5E0EN5coNDAH3IvyjNZfIfOxFIsDk3erYxsWETYYaSKtdVdYvbnlH9QZVi0ikCgYA+7lJHDNQLbSKPrhZiD+fB/Ab04Yl9ESojcY7qtRLJtfUiiSz2JF9bVgnpRV8jXtQasQYuntVkfTnXMvM+q0N9SDdT6aelgB40pts6c2pZBKhKjsrxphJKExQuL3RIjbFkKNAMfys6UuY16ur3ERGaHwWA1u+9eQSTXjTlyHBHswKBgH6Fdxu4A4RAn4jJDthuYXdA6Ek95R6AbWDJAo+2wf2rmDzqs3J2Kz2NSYBoGsOOsHPoxanpLJXWU1syp2Bn82I2aXcLCZF52RGnncVBeylgctQUsEQtm+CJ8+yjNvzLpfcYrgLNFYef2x5Lp3ZXtzRE8JIE4iQnJS6ncGCiPQi/\n-----END PRIVATE KEY-----'

const ENCRYPTION_KEY = 'yQlAy5CEMl3nOmF9X2n6i1U6PYKXV1y5';
const IV_LENGTH = 16;

const GET_ORGANISATION_ID_BY_DOMAIN = gql`
query MyQuery($_domain: String) {
  organisation_organisation(where: {domain: {_eq: $_domain}}) {
    id
  }
}`;

const cors = initMiddleware(
  Cors({
    methods: ['GET', 'POST'],
    origin: '*',
    allowedHeaders: ['Content-Type', 'Authorization'],
    exposedHeaders: ['Authorization'],
    optionsSuccessStatus: 204
  })
)

const getClient = async (info) => {
  const  token = await generateHasuraToken(info);
  console.log(token)
  return new ApolloClient({
    link: createHttpLink({
      uri: HASURA_URL,
      headers: {
        'Authorization': `Bearer ${token}`,
        'content-type': 'application/json'
      },
      fetch: fetch
    }),
    cache: new InMemoryCache({
      addTypename: false
    }),
  })
}

const getResultByServerToken = async (query, variables, client) => {
  const queryData = {
    query: query,
    variables: variables
  }
  return await client.query(queryData);
}

const saveOrUpdateByServerToken = async (query, variables, client) => {
  const mutateData = {
    mutation: query,
    variables: variables
  }
  return await client.mutate(mutateData);
}

const getOrgIdFromDomain = async (req) => {
  var host = req.headers.host;
  host = host.split(":")[0];
  if (host === 'localhost' || '0.0.0.0:8080') {
      host = 'default'
  }
  const client = await getClient({ userrole: "server", expire: 900 });
  const result = await getResultByServerToken(GET_ORGANISATION_ID_BY_DOMAIN, { _domain: host }, client);
  return result.data.organisation_organisation[0].id;
}

const generateHasuraToken = (info) => {
  const { orgId, userrole, userId, branchId, expire } = info;
  const claims = {
    'x-hasura-org-id': orgId ? orgId.toString() : '00000000-0000-0000-0000-000000000000',
    'x-hasura-allowed-roles': [userrole],
    'x-hasura-default-role': userrole,
    'x-hasura-branch-id': branchId ? branchId.toString() : '00000000-0000-0000-0000-000000000000',
    'x-hasura-user-id': userId ? userId.toString() : '00000000-0000-0000-0000-000000000000'
  }
  const hstoken = {
    'https://hasura.io/jwt/claims': claims,
    'orgId': orgId ? orgId.toString() : '00000000-0000-0000-0000-000000000000',
    'branchId': branchId ? branchId.toString() : '00000000-0000-0000-0000-000000000000',
    'userId': userId ? userId.toString() : '00000000-0000-0000-0000-000000000000'
  }
  return jwt.sign(hstoken, JWT_PRIVATE_KEY, { algorithm: 'RS256', expiresIn: expire || '720h' });
}

const encrypt = (text) => {
  let iv = crypto.randomBytes(IV_LENGTH);
  let cipher = crypto.createCipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
  let encrypted = cipher.update(text);
  encrypted = Buffer.concat([encrypted, cipher.final()]);
  return iv.toString('hex') + ':' + encrypted.toString('hex');
}

const decrypt = (text) => {
  let textParts = text.split(':');
  let iv = Buffer.from(textParts.shift(), 'hex');
  let encryptedText = Buffer.from(textParts.join(':'), 'hex');
  let decipher = crypto.createDecipheriv('aes-256-cbc', Buffer.from(ENCRYPTION_KEY), iv);
  let decrypted = decipher.update(encryptedText);
  decrypted = Buffer.concat([decrypted, decipher.final()]);
  return decrypted.toString();
}

const validateAmazonAWSToken = async (token) => {

  const jwkUrl = "https://cognito-idp." + process.env.AMPLIFY_REGION + ".amazonaws.com/" + process.env.AMPLIFY_USER_POOL_ID + "/.well-known/jwks.json"

  const getData = async url => {
    try {
      const response = await axios.get(url);
      const data = response.data;
      return data;
    } catch (err) {
    }
  }

  const jwk = await getData(jwkUrl);
  const pem = await jwkToPem(jwk.keys[0]);
  const pem1 = await jwkToPem(jwk.keys[1]);
  var email = "";
  var username = "";

  jwt.verify(token, pem, { algorithms: ['RS256'] }, function (err, decodedToken) {
    if (!err) {
      if (decodedToken.email) {
        email = decodedToken.email
      } else if (decodedToken.username) {
        username = decodedToken.username
      }
    }
  });

  jwt.verify(token, pem1, { algorithms: ['RS256'] }, function (err, decodedToken) {
    if (!err) {
      if (decodedToken.email) {
        email = decodedToken.email
      } else if (decodedToken.username) {
        username = decodedToken.username
      }
    }
  });
  const data = {
    email: email,
    username: username
  }

  return data;
}

module.exports = {
  encrypt,
  decrypt,
  generateHasuraToken,
  getResultByServerToken,
  saveOrUpdateByServerToken,
  validateAmazonAWSToken,
  getOrgIdFromDomain,
  cors,
  getClient
};
