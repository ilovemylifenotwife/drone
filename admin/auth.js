const { decrypt,getClient, generateHasuraToken, getResultByServerToken } = require('../../client/utils');
const { gql } = require("@apollo/client");

const GET_EMPLOYEE_BY_USERNAME__PASSWORD_DOMAIN = gql`
query MyQuery($_username: String) {
  organisation_employee(where: {_or: [{email_address: {_eq: $_username}}, {mobile_number: {_eq: $_username}}]}) {
    id
    roles_id
    status
    password
    branch_id
    org_id
  }
}`;

const GET_ROLES_BY_IDS = gql`
query MyQuery($_in: [uuid!]) {
  organisation_roles(where: {id: {_in: $_in}}) {
    id
    role_name
  }
}`;

const adminAuth = async (req, res) => {
  if (req.method === 'POST') {
    const { email, password } = req.body;
    const client = await getClient({ userrole: "server", expire: 900 });
    const empresult = await getResultByServerToken(GET_EMPLOYEE_BY_USERNAME__PASSWORD_DOMAIN, {
      _username: email
    }, client);
    const employee = await empresult.data.organisation_employee[0];
    if (!employee || employee === null || employee === 'undefined') {
      return res.status(200).json({ error: "Invalid Email Address" });
    }
    const decpassword = decrypt(employee.password);
    if (decpassword !== password) {
      return res.status(200).json({ error: "Invalid Password" });
    }

    const rolesresult = await getResultByServerToken(GET_ROLES_BY_IDS, {
      _in: employee.roles_id
    }, client);
    const roles = await rolesresult.data.organisation_roles;
    let userrole = "anonymous";
    let emprole = "ROLE_ANNONYMOUS";

    for (let role of roles) {
      if (role.role_name === 'ROLE_ADMIN') {
        userrole = "admin-emp";
        emprole = "ROLE_ADMIN"
        break
      } else if (role.role_name === 'ROLE_EMPLOYEE') {
        userrole = "employee";
        emprole = "ROLE_EMPLOYEE"
      } else if (role.role_name === 'ROLE_CUSTOMER') {
        return res.status(200).json({ error: "Unauthorised" });
      }
    }

    const hstoken = generateHasuraToken({
      orgId: employee.org_id,
      userrole: userrole,
      branchId: employee.branch_id,
      userId: employee.id,
    });
    const resData = {
      hstoken: hstoken,
      userId: employee.id,
      orgId: employee.org_id,
      branchId: employee.branch_id,
      role: emprole
    }
    return res.status(200).json(resData);
  } else {
    return res.status(415).json({ error: "Method not support!" });
  }
}

module.exports = adminAuth;